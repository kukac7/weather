package weather;

import org.w3c.dom.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.net.URL;

public class Weather {
	private static JFrame mainFrame;
	private static JPanel listPanel;
	private static JPanel controlPanel;
	private static JTextField input;
	private static JButton send;
	private static Document doc;

	public Weather() {
		prepareGUI();
	}

	public static void main(String[] args) {
		System.out.println("main");
		Weather layout = new Weather();
		Weather.show();
	}

	private void prepareGUI() {
		mainFrame = new JFrame();
		mainFrame.setSize(400, 400);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setLayout(new BorderLayout());
		mainFrame.setResizable(false);

		listPanel = new JPanel();
		listPanel.setLayout(new FlowLayout());

		controlPanel = new JPanel();
		controlPanel.setLayout(new FlowLayout());

		mainFrame.add(controlPanel, BorderLayout.PAGE_START);
		mainFrame.add(listPanel, BorderLayout.CENTER);
		mainFrame.setVisible(true);
	}

	private static void show() {
		System.out.println("show");
		showInputBox();
		showWeather();
	}

	private static void showInputBox() {
		final JPanel panel = new JPanel();

		JPanel inputBox = new JPanel(new FlowLayout());
		inputBox.add(new JLabel("Város:"));
		input = new JTextField("Budapest", 10);
		send = new JButton("Küldés");
		inputBox.add(input);
		inputBox.add(send);

		panel.add(inputBox);

		send.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(e.getActionCommand());
				System.out.println(input.getText());
				if (input.getText().length() > 0) {
					showWeather();
				}
				else {
					input.setBackground(Color.red);
				}
			}
		});

		controlPanel.add(panel);
		mainFrame.setVisible(true);
	}

	private static void showListBox(String node) {
		final JPanel panel = new JPanel();

		CardLayout layout = new CardLayout();
		layout.setHgap(10);
		layout.setVgap(10);
		panel.setLayout(layout);
		JPanel icons = new JPanel(new FlowLayout());
		JPanel dates = new JPanel(new FlowLayout());
		JPanel minmax = new JPanel(new FlowLayout());

		NodeList nList = doc.getElementsByTagName(node);
		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);

			System.out.println("\nCurrent Element :" + nNode.getNodeName());

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				NamedNodeMap attributes = eElement.getAttributes();

				int numAttrs = attributes.getLength();

				if (node == "symbol") {
					try {
						String path = "http://openweathermap.org/img/w/" + attributes.getNamedItem("var").getNodeValue() + ".png";
						URL iconUrl = new URL(path);
						BufferedImage image = ImageIO.read(iconUrl);
						JLabel label = new JLabel(new ImageIcon(image));
						icons.add(label);
						panel.add(icons);
					} catch (Exception exp) {
						exp.printStackTrace();
					}
				}
				else if (node == "time") {
					String date = attributes.getNamedItem("day").getNodeValue().substring(5);
					JLabel label = new JLabel(date);
					System.out.println(date);
					dates.add(label);
					panel.add(dates);
				}
				else if (node == "temperature") {
					String min = attributes.getNamedItem("min").getNodeValue();
					String max = attributes.getNamedItem("max").getNodeValue();
					JLabel label = new JLabel("");
					label.setText("<html><body style='text-align:center;'><p>min:<br>" + min + " °C</p><p>max:<br>" + max + " °C</p" +
							"></body" +
							"></html>");
					System.out.println(min);
					System.out.println(max);
					minmax.add(label);
					panel.add(minmax);
				}
			}
		}

		listPanel.add(icons);
		listPanel.add(dates);
		listPanel.add(minmax);
		mainFrame.setVisible(true);
	}

	private static void showWeather() {
		input.setBackground(Color.white);
		send.setEnabled(false);
		send.setText("Várj...");

		listPanel.removeAll();
		listPanel.revalidate();
		listPanel.repaint();

		try {
			URL url = new URL("https://api.openweathermap.org/data/2.5/forecast/daily?q=" + input.getText() + "&lang=hu_hu&mode=xml&units=metric&cnt=5&appid=68a2b3c4663814a2a5d2d8ca3d2df853");

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(url.openStream());

			doc.getDocumentElement().normalize();
			System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

			showListBox("time");
			showListBox("symbol");
			showListBox("temperature");

		} catch (Exception ex) {
			ex.printStackTrace();
			input.setBackground(Color.red);
		} finally {
			send.setEnabled(true);
			send.setText("Küldés");
		}
	}
}
